import { Injectable } from '@angular/core';
import {Observable} from "rxjs/Observable";
import {SettingsService} from "./settings.service";
import {HttpClient, HttpHeaders} from "@angular/common/http";

@Injectable()
export class UserService {

  private headers;


  constructor(private http: HttpClient,private settingsService:SettingsService) {
    this.headers = new HttpHeaders().set('Content-Type', 'application/json;charset=UTF-8')
      .set('Authorization', `Bearer ${localStorage.getItem('access_token')}`);
  }

  addUserAutomatically(userName:any):Observable<any>{
    let url = this.settingsService.queryUrls.addUserAutomatically.replace("{userName}",userName);
    return this.http.get(
      url,
      {headers:this.headers});
  }
}
