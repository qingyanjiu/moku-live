import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";

@Injectable()
export class SettingsService {

  public snapshoturl = 'http://106.75.14.33:8099/record/';
  public rtmpLiveUrl = 'rtmp://106.75.14.33/live/';
  public httpflvLiveUrl = 'http://106.75.14.33:8099/live?app=live&stream=';
  public hlsLiveUrl = 'http://106.75.14.33:8099/hls/';

  public queryUrls:any;
  private prodClientBackendUrl = 'http://106.75.14.33:38080';
  private devClientBackendUrl = 'http://localhost:8000';

  private prodClientFrontendUrl = 'http://pass.moku.site:38090';
  private devClientFrontendUrl = 'http://localhost:5000';

  private userUrls = {
    addUserAutomatically: "/user/addUserAutomatically?userName={userName}"
  }

  private liveUrls = {
    createRoom: "/live/openRoom",
    getRoomInfo: "/live/findRoomByUserName?userName={userName}"
  }

  constructor() {


    if (environment.production) {
      this.queryUrls = {
        callback: `${this.prodClientFrontendUrl}/callback`,

        addUserAutomatically: `${this.prodClientBackendUrl}` + this.userUrls.addUserAutomatically,

        createRoom: `${this.prodClientBackendUrl}` + this.liveUrls.createRoom,
        getRoomInfo: `${this.prodClientBackendUrl}` + this.liveUrls.getRoomInfo,
      }
    }
    else {
      this.queryUrls = {
        callback: `${this.devClientFrontendUrl}/callback`,

        addUserAutomatically: `${this.devClientBackendUrl}` + this.userUrls.addUserAutomatically,

        createRoom: `${this.devClientBackendUrl}` + this.liveUrls.createRoom,
        getRoomInfo: `${this.devClientBackendUrl}` + this.liveUrls.getRoomInfo,
      }
    }
  }
}
